package main

import (
	"flag"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/stackworx-public/react-static-nginx/pkg"
)

var allowMultipleInterpolationPoints = flag.Bool("allow-multiple", false, "Allow multiple interpolation points")

func main() {
	flag.Parse()

	filename := "/usr/share/nginx/html/index.html"

	if _, err := os.Stat(filename); os.IsNotExist(err) {
		log.Printf("%s does not exist", filename)
		os.Exit(-1)
	}

	data, err := ioutil.ReadFile(filename)

	if err != nil {
		log.Fatalf("Failed to read index.html: %s", err.Error())
	}

	vars, err := pkg.GetEnvVars(os.Environ())

	if err != nil {
		log.Printf("%s", err)
		os.Exit(-1)
	}

	for key := range vars {
		log.Printf("Found ENV: %s\n", key)
	}

	result, err := pkg.ReplaceEnvVars(data, vars, *allowMultipleInterpolationPoints)

	if err != nil {
		log.Printf("%s", err)
		os.Exit(-1)
	}

	err = ioutil.WriteFile(filename, result, 0644)

	if err != nil {
		log.Printf("%s", err)
		os.Exit(-1)
	}
}
